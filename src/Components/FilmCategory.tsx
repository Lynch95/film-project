import { FormControl, Select } from '@chakra-ui/react'
import React from 'react'
import categories from '../Categories'

interface Props {
    ChosenCategory: (category: string) => void
}

const FilmCategory = ({ChosenCategory}: Props) => {
  return (
    
        <FormControl>
            <Select placeholder='All Categories' onChange={(event) => ChosenCategory(event.target.value)}>
{categories.map(category => <option key={category} value={category}>{category}</option>)}
</Select>
        </FormControl>
    
  )
}

export default FilmCategory