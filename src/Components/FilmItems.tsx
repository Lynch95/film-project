import { Grid, GridItem } from '@chakra-ui/react'
import React, { useState } from 'react'

interface FilmItems {
  id: number
  title: string
  description: string
  Image: string
  category: string
}

interface Props {
  FilmItem: FilmItems[]
  ItemClick: (id: number) => void
  selectedId: number | null
}

const FilmItems = ({FilmItem, ItemClick, selectedId}: Props) => {


  return (
<Grid templateColumns='repeat(4, 1fr)' gap={6} margin={6}>
{FilmItem.map(item => <GridItem w='100%' h='300px' border='1px' borderColor='white' key={item.id} onClick={() => ItemClick(item.id)}>
  <img src={item.Image} style={{width: '100%', height: '100%'}} />
{selectedId === item.id && (
  <>
  <div>{item.title}</div>
  <div>{item.description}</div>
  </>
)}
</GridItem>)}
  
</Grid>
  )
}

export default FilmItems