import React from 'react'
import { Box } from '@chakra-ui/react'

const NavBar = () => {
  return (
    <Box bg='tomato' w='100%' p={4} color='white'>
  Barry's Film Reviews
</Box>
  )
}

export default NavBar