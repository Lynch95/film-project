import React, { useState } from 'react'
import NavBar from './Components/NavBar'
import FilmItems from './Components/FilmItems'
import Footer from './Components/Footer'
import FilmCategory from './Components/FilmCategory'


function App() {

const [selectedFilm, setSelectedFilm] = useState<number | null>(null)
const [category, setCategory] = useState<string>('')
const [items, setItems] = useState([
  {id: 1, title: 'The Fall of the house of Usher', description: 'review of film A', category: 'Horror', Image: '/images/usher.jpg'},
  {id: 2, title: 'The Fall of the house of Usher', description: 'review of film B', category: 'Horror', Image: '/images/usher.jpg'},
  {id: 3, title: 'The Fall of the house of Usher', description: 'review of film C', category: 'Comedy', Image: '/images/usher.jpg'},
  {id: 4, title: 'The Fall of the house of Usher', description: 'review of film D', category: 'Action', Image: '/images/usher.jpg'},
  {id: 5, title: 'The Fall of the house of Usher', description: 'review of film E', category: 'Horror', Image: '/images/usher.jpg'},
  {id: 6, title: 'The Fall of the house of Usher', description: 'review of film F', category: 'Horror', Image: '/images/usher.jpg'},
  {id: 7, title: 'The Fall of the house of Usher', description: 'review of film G', category: 'Horror', Image: '/images/usher.jpg'},
  {id: 8, title: 'The Fall of the house of Usher', description: 'review of film H', category: 'Horror', Image: '/images/usher.jpg'},
])


const FilmClicked = (id: number) => {
  setSelectedFilm(id)
  setItems(items.filter(item => item.id === id))
}

const visibleFilms = category ? items.filter(item => item.category === category) : items

return (

    <>
    <NavBar />
    <FilmCategory ChosenCategory={(category) => setCategory(category)} />
    <FilmItems FilmItem={visibleFilms} ItemClick={FilmClicked} selectedId={selectedFilm} />
    
    </>
  )
}

export default App